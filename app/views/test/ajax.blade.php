<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>To do List</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">

  </head>
  <body>
    
    <div class="container">
  		<div class="row clearfix">
  			<div class="col-md-6 col-md-offset-3">
  				<h2 style="text-align:center;"> To do List </h2>  				

  			</div>
  		</div>

      <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">
                  Add Task
                </h3>
              </div>
              <div class="panel-body">
                @foreach($tasks as $task)
                    <table class="table" id="task_list">
                      <thead>                        
                        <tr>Title</tr>
                        <tr></tr>
                        <tr></tr>
                      </thead>
                      <tbody> 
                        @if($task->status)
                          <tr class="success" id="{{ $task->id }}">
                            <td id="title_{{ $task->id }}">{{ $task->title }}</td>
                            <td>Task Done</td>
                            <td><a href="#" onClick="delete_task('{{ $task->id }}');" class="btn btn-warning"><i class="icon-white icon-pencil"></i> Edit</a></td>
                            <td><a href="#" onClick="edit_task('{{ $task->id }}','{{ $task->title }}');" class="btn btn-danger"><i class="icon-white icon-remove"></i> Delete</a></td>
                          </tr>
                        @else
                          <tr id="{{ $task->id }}">
                            <td id="title_{{ $task->id }}">{{ $task->title }}</td>
                            <td><a href="#" onClick="task_done('{{ $task->id }}');" class="btn btn-info"><i class="icon-white icon-ok"></i> Task Done</a></td>
                            <td><a href="#" onClick="delete_task('{{ $task->id }}');" class="btn btn-warning"><i class="icon-white icon-pencil"></i> Edit</a></td>
                            <td><a href="#" onClick="edit_task('{{ $task->id }}','{{ $task->title }});" class="btn btn-danger"><i class="icon-white icon-remove"></i> Delete</a></td>
                          </tr>
                        @endif
                      </tbody>
                    </table>
                  @endforeach
              </div>              
            </div>
  	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
  </body>
</html>